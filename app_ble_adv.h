/***************************************************************************//**
 * @file
 * @brief app_ble_adv.h
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef _APP_BLE_ADV_H_
#define _APP_BLE_ADV_H_

/****************************************************************************/
/* Advertisement data                                                       */
/****************************************************************************/

#define APP_BLE_ADV_ADV_FLAGS_LENGTH                   2
#define APP_BLE_ADV_ADV_FLAGS_TYPE                     0x01

#define APP_BLE_ADV_SERVICE_DATA_LENGTH                4
#define APP_BLE_ADV_SERVICE_DATA_TYPE                  0x16
#define APP_BLE_ADV_BATTERY_SERVICE                    0x180F
#define APP_BLE_ADV_USERDATA_SERVICE                   0x1801

/** Complete local name. */
#define APP_BLE_ADV_TYPE_LOCAL_NAME                    0x09

/* Bit mask for flags advertising data type. */
#define APP_BLE_ADV_ADV_FLAGS_LE_LIMITED_DISCOVERABLE  0x01
#define APP_BLE_ADV_ADV_FLAGS_LE_GENERAL_DISCOVERABLE  0x02
#define APP_BLE_ADV_ADV_FLAGS_BR_EDR_NOT_SUPPORTED     0x04

#define APP_BLE_ADV_DEVICE_NAME_LENGTH                 4
#define APP_BLE_ADV_DEVICE_NAME			                   "Loki"

/****************************************************************************/
/* Structure that holds Scan Response Data                                  */
/****************************************************************************/
typedef struct __APP_BLE_ADV {
  uint8_t flagsLength;                 /**< Length of the Flags field. */
  uint8_t flagsType;                   /**< Type of the Flags field. */
  uint8_t flags;                       /**< Flags field. */
  uint8_t serviceDataLength;           /**< Service data length */
  uint8_t serviceDataType;             /**< Service data type */
  uint8_t batteryDataUUID[2];          /**< Service UUID */
  uint8_t batteryData;                 /**< Service data value */
  uint8_t serviceData2Length;          /**< Service data length */
  uint8_t serviceData2Type;            /**< Service data type */
  uint8_t userDataUUID[2];             /**< Service UUID */
  int8_t userData[2];                  /**< Service data value */
  uint8_t localNameLength;             /**< Length of the local name field. */
  uint8_t localNameType;               /**< Type of the local name field. */
  uint8_t localName[APP_BLE_ADV_DEVICE_NAME_LENGTH];  /**< Local name field. */
} APP_BLE_ADV;

#endif
