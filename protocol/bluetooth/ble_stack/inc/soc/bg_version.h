/***************************************************************************//**
 * @brief Bluetooth stack version definition
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BT_VERSION_H
#define SL_BT_VERSION_H

#define BG_VERSION_MAJOR 2
#define BG_VERSION_MINOR 13
#define BG_VERSION_PATCH 1
#define BG_VERSION_BUILD 175
#define BG_VERSION_HASH  {0xbf,0xda,0x80,0xf2,0x76,0xe7,0x2f,0x86,0x48,0x07,0x17,0x32,0x91,0x11,0x24,0x0e,0x08,0x1b,0x93,0x09}

#endif
