#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "em_device.h"
#include "em_cmu.h"

#include "app.h"
#include "app_ble_adv.h"
#include "accel.h"
#include "batt.h"

#define BATT_UPDATE_TIMER_ID     1
#define BATT_UPDATE_TIMER_RATE   14400  // sec (3 hours)

#define INVERSION_TIMER_ID       2
#define INVERSION_TIMER_RATE     4      // sec

#define INVERSION_ENTER_THRESH   -20    // deg
#define INVERSION_EXIT_THRESH    -10    // deg
#define INVERSION_DEBOUNCE_COUNT 6

APP_BLE_ADV advData;
uint16_t advRate       = 4000;  // ms
uint16_t actualAdvRate = 6400;  // (advRate / 0.625)

int8_t inversionPitch = 0;
int8_t inversionDebounce = 0;
bool inversionState = false;

uint8_t battery_voltage_percentage = 0;

static uint8_t boot_to_dfu = 0;

static void startAdvertising();

static void updateAdvData()
{
  advData.flagsLength         = APP_BLE_ADV_ADV_FLAGS_LENGTH;
  advData.flagsType           = APP_BLE_ADV_ADV_FLAGS_TYPE;
  advData.flags               = APP_BLE_ADV_ADV_FLAGS_LE_GENERAL_DISCOVERABLE | APP_BLE_ADV_ADV_FLAGS_BR_EDR_NOT_SUPPORTED;
  advData.serviceDataLength   = APP_BLE_ADV_SERVICE_DATA_LENGTH;
  advData.serviceDataType     = APP_BLE_ADV_SERVICE_DATA_TYPE;
  advData.batteryDataUUID[0]  = (APP_BLE_ADV_BATTERY_SERVICE & 0xFF);
  advData.batteryDataUUID[1]  = ((APP_BLE_ADV_BATTERY_SERVICE >> 8)& 0xFF);
  advData.batteryData         = battery_voltage_percentage;
  advData.serviceData2Length  = APP_BLE_ADV_SERVICE_DATA_LENGTH + 1;
  advData.serviceData2Type    = APP_BLE_ADV_SERVICE_DATA_TYPE;
  advData.userDataUUID[0]     = (APP_BLE_ADV_USERDATA_SERVICE & 0xFF);
  advData.userDataUUID[1]     = ((APP_BLE_ADV_USERDATA_SERVICE >> 8)& 0xFF);
  advData.userData[0]         = inversionState;
  advData.userData[1]         = inversionPitch;
  advData.localNameLength     = sizeof(APP_BLE_ADV_DEVICE_NAME);
  advData.localNameType       = APP_BLE_ADV_TYPE_LOCAL_NAME;
  strncpy((char*)advData.localName, APP_BLE_ADV_DEVICE_NAME, APP_BLE_ADV_DEVICE_NAME_LENGTH);

  gecko_cmd_le_gap_bt5_set_adv_data(0, 0, sizeof(advData), (uint8_t*)(&advData));
}

static void startAdvertising()
{
  /* Set advertising parameters. 100ms advertisement interval.
   * The first parameter is advertising set handle
   * The next two parameters are minimum and maximum advertising interval, both in
   * units of (milliseconds * 1.6).
   * The last two parameters are duration and maxevents left as default. */
  gecko_cmd_le_gap_set_advertise_timing(0, actualAdvRate, actualAdvRate, 0, 0);

  /* Start general advertising and enable connections. */
  gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_non_connectable);
}

/* Main application */
void appMain(gecko_configuration_t *pconfig)
{
#if DISABLE_SLEEP > 0
  pconfig->sleep.flags = 0;
#endif

  bool loggedIn = false;

  /* Initialize stack */
  gecko_init(pconfig);

  // Set +10 dBm Transmit Power (max without AFH)
  gecko_cmd_system_set_tx_power(100);

  // Init ADC and start battery update timer
  initBatteryADC();
  gecko_cmd_hardware_set_lazy_soft_timer(TIMER_SEC_TO_TICK(BATT_UPDATE_TIMER_RATE), TIMER_SEC_TO_TICK(300), BATT_UPDATE_TIMER_ID, false);

  // Init accel and start inversion sense timer
  initAccelSPI();
  gecko_cmd_hardware_set_lazy_soft_timer(TIMER_SEC_TO_TICK(INVERSION_TIMER_RATE), TIMER_SEC_TO_TICK(INVERSION_TIMER_RATE), INVERSION_TIMER_ID, false);

  while (1)
  {
    /* Event pointer for handling events */
    struct gecko_cmd_packet* evt;

    /* Check for stack event. This is a blocking event listener. If you want non-blocking please see UG136. */
    evt = gecko_wait_event();

    /* Handle events */
    switch (BGLIB_MSG_ID(evt->header))
    {
      /* This boot event is generated when the system boots up after reset.
       * Do not call any stack commands before receiving the boot event.
       * Here the system is set to start advertising immediately after boot procedure. */
      case gecko_evt_system_boot_id:
      {
        battery_voltage_percentage = getBatteryPercent();
        updateAdvData();
        startAdvertising();
      }
      break;

      case gecko_evt_le_connection_opened_id:
      break;

      case gecko_evt_le_connection_closed_id:
      {
        /* Check if need to boot to OTA DFU mode */
        if (boot_to_dfu)
        {
          /* Enter to OTA DFU mode */
          gecko_cmd_system_reset(2);
        }
        else
        {
          loggedIn = false;
          /* Restart advertising after client has disconnected */
          updateAdvData();
          startAdvertising();
        }
      }
      break;

      case gecko_evt_hardware_soft_timer_id:
      {
        switch (evt->data.evt_hardware_soft_timer.handle)
        {
          case BATT_UPDATE_TIMER_ID:
          {
            battery_voltage_percentage = getBatteryPercent();
            updateAdvData();
          }
          break;

          case INVERSION_TIMER_ID:
          {
            inversionPitch = calcAccelPitchSPI();

            if (inversionState == true)
            {
              inversionDebounce += (inversionPitch < INVERSION_EXIT_THRESH ? 1 : -1);
            }
            else
            {
              inversionDebounce += (inversionPitch < INVERSION_ENTER_THRESH ? 1 : -1);
            }

            if (inversionDebounce <= 0)
            {
              inversionState = false;
              inversionDebounce = 0;
            }
            else if (inversionDebounce >= INVERSION_DEBOUNCE_COUNT)
            {
              inversionState = true;
              inversionDebounce = INVERSION_DEBOUNCE_COUNT;
            }

            updateAdvData();
          }
          break;

          default:
          break;
        }
      }
      break;

      case gecko_evt_gatt_server_user_read_request_id:
      {
        switch (evt->data.evt_gatt_server_user_read_request.characteristic)
        {
          case gattdb_battery_level:
          {
            battery_voltage_percentage = getBatteryPercent();
            updateAdvData();
            gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection, gattdb_battery_level, 0, sizeof(battery_voltage_percentage), &battery_voltage_percentage);
          }
          break;

          case gattdb_password:
          {
            gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection, gattdb_password, 0, sizeof(loggedIn), (const uint8_t*)loggedIn);
          }

//          case gattdb_inversion_threshold:
//          {
//            gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection, gattdb_inversion_threshold, 0, sizeof(inversionThreshold), (const uint8_t*)&inversionThreshold);
//          }
//
//          case gattdb_inversion_debounce:
//          {
//            gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection, gattdb_inversion_debounce, 0, sizeof(inversionDebounceCount), (const uint8_t*)&inversionDebounceCount);
//          }

          default:
          break;
        }
      }
      break;

      case gecko_evt_gatt_server_user_write_request_id:
      {
        switch (evt->data.evt_gatt_server_user_write_request.characteristic)
        {
          case gattdb_password:
          {
            uint16_t password = 0;
            if (evt->data.evt_gatt_server_user_write_request.value.len <= sizeof(password))
            {
              memcpy(&password, evt->data.evt_gatt_server_user_write_request.value.data, evt->data.evt_gatt_server_user_write_request.value.len);
              if (password == 9510 || password == 0x1095)
              {
                loggedIn = true;
                gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_password, bg_err_success);
              }
              else gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_password, (uint8)bg_err_att_value_not_allowed);
            }
            else gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_password, (uint8)bg_err_att_invalid_att_length);
          }
          break;

//          case gattdb_inversion_threshold:
//          {
//            if (loggedIn)
//            {
//              inversionThreshold = evt->data.evt_gatt_server_user_write_request.value.data[0];
//              gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_inversion_threshold, bg_err_success);
//            }
//            else gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_inversion_threshold, (uint8)bg_err_att_write_not_permitted);
//          }
//          break;
//
//          case gattdb_inversion_debounce:
//          {
//            if (loggedIn)
//            {
//              inversionDebounceCount = evt->data.evt_gatt_server_user_write_request.value.data[0];
//              gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_inversion_debounce, bg_err_success);
//            }
//            else gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_inversion_debounce, (uint8)bg_err_att_write_not_permitted);
//          }
//          break;

//          case gattdb_ota_control:
//          {
//            if (loggedIn)
//            {
//              /* Set flag to enter to OTA mode */
//              boot_to_dfu = 1;
//              /* Send response to Write Request */
//              gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_ota_control, bg_err_success);
//              /* Close connection to enter to DFU OTA mode */
//              gecko_cmd_le_connection_close(evt->data.evt_gatt_server_user_write_request.connection);
//            }
//            else gecko_cmd_gatt_server_send_user_write_response(evt->data.evt_gatt_server_user_write_request.connection, gattdb_ota_control, (uint8)bg_err_att_write_not_permitted);
//          }
//          break;

          default:
          break;
        }
      }
      break;

      default:
      break;
    }
  }
}

