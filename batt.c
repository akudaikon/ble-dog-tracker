#include "bg_types.h"
#include "native_gecko.h"
#include "em_cmu.h"
#include "em_adc.h"
#include "batt.h"

static VoltageCapacityPair battCR2032Model[] = { { 3000, 100 }, { 2900, 80 }, { 2800, 60 }, { 2700, 40 }, { 2600, 30 }, { 2500, 20 }, { 2400, 10 }, { 2000, 0 } };

void initBatteryADC()
{
  ADC_Init_TypeDef adcInit = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef adcSingleInit = ADC_INITSINGLE_DEFAULT;

  adcSingleInit.acqTime = adcAcqTime16;
  adcSingleInit.reference = adcRef5V;
  adcSingleInit.posSel = adcPosSelAVDD;
  adcSingleInit.negSel = adcNegSelVSS;

  CMU_ClockEnable(cmuClock_ADC0, true);
  ADC_Init(ADC0, &adcInit);
  ADC_InitSingle(ADC0, &adcSingleInit);
  CMU_ClockEnable(cmuClock_ADC0, false);
}

uint8_t getBatteryPercent()
{
  CMU_ClockEnable(cmuClock_ADC0, true);

  ADC_Start(ADC0, adcStartSingle);
  while ((ADC_IntGet(ADC0) & ADC_IF_SINGLE) != ADC_IF_SINGLE);

  uint32_t battery_voltage = (ADC_DataSingleGet(ADC0) * 5000 / 4096);

  CMU_ClockEnable(cmuClock_ADC0, false);

  return calculateBatteryLevel(battery_voltage, battCR2032Model, sizeof(battCR2032Model) / sizeof(VoltageCapacityPair));
}

uint8_t calculateBatteryLevel(uint16_t voltage, VoltageCapacityPair *model, uint8_t modelEntryCount)
{
  uint8_t res = 0;
  int i;
  int32_t tmp;

  for (i = 0; i < modelEntryCount; i++)
  {
    if (i == modelEntryCount)
    {
      return model[i].capacity;
    }
    else if (voltage >= model[i].voltage)
    {
      res = model[i].capacity;
      break;
    }
    else if ((voltage < model[i].voltage) && (voltage >= model[i + 1].voltage))
    {
      //
      tmp = (voltage - model[i].voltage)
            * (model[i + 1].capacity - model[i].capacity)
            / (model[i + 1].voltage - model[i].voltage);
      tmp += model[i].capacity;
      res = tmp;
      break;
    }
  }

  return res;
}
