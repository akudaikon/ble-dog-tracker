#ifndef ACCEL_H
#define ACCEL_H

#include "em_gpio.h"

#define LIS3DH_I2C_ADDR         0x18

#define LIS3DH_REG_CTRL1        0x20
#define LIS3DH_REG_CTRL2        0x21
#define LIS3DH_REG_CTRL3        0x22
#define LIS3DH_REG_CTRL4        0x23
#define LIS3DH_REG_CTRL5        0x24
#define LIS3DH_REG_CTRL6        0x25

#define LIS3DH_REG_FIFOCTRL     0x2E
#define LIS3DH_REG_FIFOSRC      0x2F

#define LIS3DH_REG_OUT_X_L      0x28
#define LIS3DH_REG_OUT_X_H      0x29
#define LIS3DH_REG_OUT_Y_L      0x2A
#define LIS3DH_REG_OUT_Y_H      0x2B
#define LIS3DH_REG_OUT_Z_L      0x2C
#define LIS3DH_REG_OUT_Z_H      0x2D

#define I2C_SCL_PORT            gpioPortF
#define I2C_SCL_PIN             7
#define I2C_SDA_PORT            gpioPortF
#define I2C_SDA_PIN             6

#define SPI_CS_PORT             gpioPortC
#define SPI_CS_PIN              10
#define SPI_CLK_PORT            gpioPortF
#define SPI_CLK_PIN             7
#define SPI_MOSI_PORT           gpioPortF
#define SPI_MOSI_PIN            6
#define SPI_MISO_PORT           gpioPortC
#define SPI_MISO_PIN            9

void initAccelI2T();
int8_t calcAccelPitchI2T();

void initAccelSPI();
int8_t calcAccelPitchSPI();

#endif
