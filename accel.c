#include "bg_types.h"
#include "native_gecko.h"
#include "math.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "i2cspm.h"
#include "accel.h"

#define NOP_DELAY()   __NOP();

static I2C_TransferReturn_TypeDef i2cWriteByte(uint16_t addr, uint8_t command, uint8_t data)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef sta;
  uint8_t                    i2c_write_command[1];
  uint8_t                    i2c_write_data[1];

  seq.addr  = addr << 1;
  seq.flags = I2C_FLAG_WRITE_WRITE;

  /* Select command to issue */
  i2c_write_command[0] = command;
  seq.buf[0].data   = i2c_write_command;
  seq.buf[0].len    = 1;

  /* Select location/length of data to write */
  i2c_write_data[0] = data;
  seq.buf[1].data = i2c_write_data;
  seq.buf[1].len  = 1;

  sta = I2CSPM_Transfer(I2C0, &seq);
  if (sta != i2cTransferDone)
  {
    return sta;
  }

  return sta;
}

static I2C_TransferReturn_TypeDef i2cReadByte(uint16_t addr, uint8_t command, uint8_t *val)
{
  I2C_TransferSeq_TypeDef    seq;
  I2C_TransferReturn_TypeDef sta;
  uint8_t                    i2c_write_data[1];
  uint8_t                    i2c_read_data[1];

  seq.addr  = addr << 1;
  seq.flags = I2C_FLAG_WRITE_READ;
  /* Select command to issue */
  i2c_write_data[0] = command;
  seq.buf[0].data   = i2c_write_data;
  seq.buf[0].len    = 1;
  /* Select location/length of data to be read */
  seq.buf[1].data = i2c_read_data;
  seq.buf[1].len  = 1;

  sta = I2CSPM_Transfer(I2C0, &seq);
  if (sta != i2cTransferDone)
  {
    return sta;
  }
  if (NULL != val)
  {
    *val = i2c_read_data[0];
  }
  return sta;
}

void initAccelI2T()
{
  CMU_ClockEnable(cmuClock_I2C0, true);
  GPIO_PinModeSet(I2C_SDA_PORT, I2C_SDA_PIN, gpioModeWiredAndPullUp, 1);     // I2C SDA
  GPIO_PinModeSet(I2C_SCL_PORT, I2C_SCL_PIN, gpioModeWiredAndPullUp, 1);     // I2C SCL

  // Enable pins at location 30 as specified in datasheet
  I2C0->ROUTEPEN = I2C_ROUTEPEN_SDAPEN | I2C_ROUTEPEN_SCLPEN;
  I2C0->ROUTELOC0 = (I2C0->ROUTELOC0 & (~_I2C_ROUTELOC0_SDALOC_MASK)) | I2C_ROUTELOC0_SDALOC_LOC30;
  I2C0->ROUTELOC0 = (I2C0->ROUTELOC0 & (~_I2C_ROUTELOC0_SCLLOC_MASK)) | I2C_ROUTELOC0_SCLLOC_LOC30;

  I2CSPM_Init_TypeDef i2cInit = I2CSPM_INIT_DEFAULT;
  i2cInit.port                = I2C0;
  i2cInit.sdaPort             = gpioPortF;
  i2cInit.sdaPin              = 6;
  i2cInit.sclPort             = gpioPortF;
  i2cInit.sclPin              = 7;
  i2cInit.portLocationSda     = 30;
  i2cInit.portLocationScl     = 30;
  I2CSPM_Init(&i2cInit);

  i2cWriteByte(LIS3DH_I2C_ADDR, LIS3DH_REG_CTRL5, 0x80);  // reboot memory
  i2cWriteByte(LIS3DH_I2C_ADDR, LIS3DH_REG_CTRL1, 0x1F);  // 1Hz, low power mode, XYZ enabled
  //i2cWriteByte(LIS3DH_I2C_ADDR, LIS3DH_REG_CTRL4, 0x00);  // +/-2g range
  CMU_ClockEnable(cmuClock_I2C0, false);
}

int8_t calcAccelPitchI2T()
{
   static const float alpha = 0.4;
   static float fX, fY, fZ = 0;
   uint8_t x, y, z = 0;

   // read x, y, z
   CMU_ClockEnable(cmuClock_I2C0, true);
   i2cReadByte(LIS3DH_I2C_ADDR, LIS3DH_REG_OUT_X_H, &x);
   i2cReadByte(LIS3DH_I2C_ADDR, LIS3DH_REG_OUT_Y_H, &y);
   i2cReadByte(LIS3DH_I2C_ADDR, LIS3DH_REG_OUT_Z_H, &z);
   CMU_ClockEnable(cmuClock_I2C0, false);

   // low pass filter
   fX = (int8_t)x * alpha + (fX * (1.0 - alpha));
   fY = (int8_t)y * alpha + (fY * (1.0 - alpha));
   fZ = (int8_t)z * alpha + (fZ * (1.0 - alpha));

   // calculate pitch
   return (int8_t)((atan2(fX, sqrt(fY * fY + fZ * fZ)) * 180.0) / 3.1415); //M_PI;
}

static void spiWriteByte(uint8_t command, uint8_t data)
{
  GPIO_PinOutClear(SPI_CS_PORT, SPI_CS_PIN);
  NOP_DELAY();

  // do not set R/W bit and MS bit (write and no multiple write)

  // Write command
  for (uint8_t b = 0; b < 8; b++)
  {
    GPIO_PinOutClear(SPI_CLK_PORT, SPI_CLK_PIN);
    if ((command << b) & 0x80) GPIO_PinOutSet(SPI_MOSI_PORT, SPI_MOSI_PIN);
    else GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);
    NOP_DELAY();
    GPIO_PinOutSet(SPI_CLK_PORT, SPI_CLK_PIN);
    NOP_DELAY();
  }

  // Write data
  for (uint8_t b = 0; b < 8; b++)
  {
    GPIO_PinOutClear(SPI_CLK_PORT, SPI_CLK_PIN);
    if (data & 0x80) GPIO_PinOutSet(SPI_MOSI_PORT, SPI_MOSI_PIN);
    else GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);
    NOP_DELAY();
    GPIO_PinOutSet(SPI_CLK_PORT, SPI_CLK_PIN);
    NOP_DELAY();
    data <<= 1;
  }

  GPIO_PinOutSet(SPI_CS_PORT, SPI_CS_PIN);
  GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);
  NOP_DELAY();
}

static uint8_t spiReadByte(uint8_t command)
{
  uint8_t data = 0;
  GPIO_PinOutClear(SPI_CS_PORT, SPI_CS_PIN);
  NOP_DELAY();

  command |= 0x80; // set R/W bit, do not set MS bit (read and no multiple read)

  // Write command
  for (uint8_t b = 0; b < 8; b++)
  {
    GPIO_PinOutClear(SPI_CLK_PORT, SPI_CLK_PIN);
    if ((command << b) & 0x80) GPIO_PinOutSet(SPI_MOSI_PORT, SPI_MOSI_PIN);
    else GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);
    NOP_DELAY();
    GPIO_PinOutSet(SPI_CLK_PORT, SPI_CLK_PIN);
    NOP_DELAY();
  }

  // Read data
  for (uint8_t b = 0; b < 8; b++)
  {
    GPIO_PinOutClear(SPI_CLK_PORT, SPI_CLK_PIN);
    NOP_DELAY();
    data |= GPIO_PinInGet(SPI_MISO_PORT, SPI_MISO_PIN);
    GPIO_PinOutSet(SPI_CLK_PORT, SPI_CLK_PIN);
    NOP_DELAY();
    data <<= 1;
  }

  GPIO_PinOutSet(SPI_CS_PORT, SPI_CS_PIN);
  GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);
  NOP_DELAY();
  return data;
}

void initAccelSPI(void)
{
  CMU_ClockEnable(cmuClock_GPIO, true);

  GPIO_PinModeSet(SPI_CLK_PORT, SPI_CLK_PIN, gpioModePushPull, 0);    // CLK is push pull
  GPIO_PinModeSet(SPI_CS_PORT, SPI_CS_PIN, gpioModePushPull, 1);      // CS is push pull
  GPIO_PinModeSet(SPI_MOSI_PORT, SPI_MOSI_PIN, gpioModePushPull, 1);  // MOSI is push pull
  GPIO_PinModeSet(SPI_MISO_PORT, SPI_MISO_PIN, gpioModeInput, 1);     // MISO is input

  GPIO_PinOutSet(SPI_CS_PORT, SPI_CS_PIN);
  GPIO_PinOutSet(SPI_CLK_PORT, SPI_CLK_PIN);
  GPIO_PinOutClear(SPI_MOSI_PORT, SPI_MOSI_PIN);

  spiWriteByte(LIS3DH_REG_CTRL5, 0x80);  // reboot memory
  spiWriteByte(LIS3DH_REG_CTRL1, 0x1F);  // 1Hz, low power mode, XYZ enabled
}

int8_t calcAccelPitchSPI()
{
   static const float alpha = 0.8;
   static float fX, fY, fZ = 0;
   uint8_t x, y, z = 0;

   // read x, y, z
   x = spiReadByte(LIS3DH_REG_OUT_X_H);
   y = spiReadByte(LIS3DH_REG_OUT_Y_H);
   z = spiReadByte(LIS3DH_REG_OUT_Z_H);

   // low pass filter
   fX = (int8_t)x * alpha + (fX * (1.0 - alpha));
   fY = (int8_t)y * alpha + (fY * (1.0 - alpha));
   fZ = (int8_t)z * alpha + (fZ * (1.0 - alpha));

   // calculate pitch
   //roll = (int8_t)((atan2(-fY, fZ) * 180.0) / 3.1415); // M_PI
   return (int8_t)((atan2(fX, sqrt(fY * fY + fZ * fZ)) * 180.0) / 3.1415); //M_PI;
}
