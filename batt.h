#ifndef BATT_H
#define BATT_H

typedef struct {
  uint16_t      voltage;
  uint8_t       capacity;
} VoltageCapacityPair;

void initBatteryADC();
uint8_t getBatteryPercent();
uint8_t calculateBatteryLevel(uint16_t voltage, VoltageCapacityPair *model, uint8_t modelEntryCount);

#endif
